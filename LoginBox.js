import React, { Component } from 'react';
import { Link } from 'react-router';
import { Auth } from './../../../auth.core';

/** COMPONENTS CUSTOM **/
// import { nextStorage } from '../../../util/nextStorage';
import { APP_CONFIG } from '../../../boot.config';
export class LoginBox extends Component{
  constructor(props){
    super(props);

    this.renderUserInfos = this.renderUserInfos.bind(this);
    this.renderUserHeader = this.renderUserHeader.bind(this);
  }

  renderUserInfos(){
    if(this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR){
      return (
        <ul>
          <li><Link to={'/dashboard'}>Painel de Controle</Link></li>
          <li><Link to={'/profile'}>Meus Pedidos</Link></li>
          <li><Link to={'/profile'}>Meus Chamados</Link></li>
          <li><Link to={'/profile'}>Central de Atendimento</Link></li>
        </ul>
      );
    }
  }//renderUserInfos();

  renderUserHeader(){
    if(this.props.userHover){
      if(this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR){
        return (
          <div className={'userHover'}>
            <h2>Olá Visitante, (sair)</h2>
            {this.renderUserInfos()}
          </div>
        );
      }else{
        return (
          <div className={'userHover'}>
            <h2>Olá Visitante, faça seu Login</h2>
            <Auth
              modeForm={'horizontal'}
              currentUser={this.props.currentUser}
              hoverLogin
            />
          </div>
        );
      }
    }
  }//renderUserHeader();

  render(){
    return (
      <div>{this.renderUserHeader()}</div>
    );
  }
}
